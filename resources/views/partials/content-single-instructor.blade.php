<?php /* Initiate the Pods Object */
    // get the current slug
    global $post;
  	// get pods object
    // $post->post_type can be used instead of the pod name, ie 'video'
    $mypod = pods( $post->post_type, $post->ID );
    
    //var_dump($post);
?>

<article @php post_class() @endphp>

  <div class="page-header">
    <h1>{{ $post->post_title }}</h1>
  </div>

  
  <div class="the-content">
    {{ the_content() }}
  </div>


</article>
