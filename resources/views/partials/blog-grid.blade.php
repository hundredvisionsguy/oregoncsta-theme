@php
  $divclass = ' '.$class;
@endphp
  <div class="row">
    <header class="col-12 blog-grid-title">
      <h2 class="home-news">News</h2>
    </header>
  </div>
  <div class="row">
  @foreach($news_loop as $news_item)
    <div class="col-sm-12 col-md-4 blog-grid">
      <div class="grid-box">
        <div class="grid-background" style="background-image: url('{!! $news_item['thumbnail'] !!}');"></div>
        <div class="grid-foreground blog-grid-foreground">
          <p>{!! $news_item['link'] !!}{!! $news_item['title'] !!}</a></p>
        </div>
      </div>
    </div>
  @endforeach
</div>
