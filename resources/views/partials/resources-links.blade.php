<div class="home-content-container row">
  <div class="home-content-nav closed row justify-content-between">
    <div class="col-9 col-md-12">
      <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
	       <div class="flipper">
           @php dynamic_sidebar('sidebar-resources') @endphp
         </div>
       </div>
    </div>
  </div>
</div>
