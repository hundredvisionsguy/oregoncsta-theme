@php
  $divclass = ' '.$class;

  //avoid single hanging box by packing in 4 cols if the number of items just overflows three columns
  $num_events = count($events_loop) + 1;
  $cols = 'col-md-4';
  if($num_events % 3 == 1)
    $cols = 'col-md-3';

@endphp
  <div class="row">
    <header class="col-12 blog-grid-title">
      <h2 class="home-news">Events</h2>
    </header>
  </div>
  <div class="row">
  @foreach($events_loop as $events_item)
    <div class="col-sm-12 {!! $cols !!} blog-grid">
      <div class="grid-box">
        <div class="grid-background" style="background-image: url('{!! $events_item['thumbnail'] !!}');"></div>
        <div class="grid-foreground event-grid-foreground">
          <p>{!! $events_item['link'] !!}{!! $events_item['title'] !!}</a></p>
        </div>
      </div>
    </div>
  @endforeach
  
  <div class="col-sm-12 {!! $cols !!} blog-grid">
      <div class="grid-box">
        <div class="grid-background" style="background-image: url('{!! $events_item['thumbnail'] !!}');"></div>
        <div class="grid-foreground event-grid-foreground">
          <p><a href='{!! get_home_url() !!}/events/'>More Events</a></p>
        </div>
      </div>
  </div>

</div>
