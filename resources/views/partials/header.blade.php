<header class="banner">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-3 brand-container">
          <div class="brand">
            <a class="brand text-hide" href="{{ home_url('/') }}"><img class="brand-img" alt="Oregon CSTA logo" src="@asset('images/OregonCSTA_logo_small_white.png')"></a>
          </div>
          <div class="menu-toggle">
            <img class="menu-toggle" src="@asset('images/three-bars_white.svg')" alt="toggle menu">
          </div>
      </div>
      <nav class="nav-primary col-xs-12 col-md-9">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
    </div>
  </div>
</header>
