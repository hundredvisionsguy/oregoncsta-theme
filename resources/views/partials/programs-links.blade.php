<div class="home-content-container bg-primary row">
  <div class="home-content-nav closed row justify-content-between">
    <div class="col-9 col-md-12">
      <h3 class="programs-link"><a href="category/programs">Programs</a></h3>
    </div>
    <div class="col-3 col-md-12">
      <button id="programsToggle" class="float-right toggle programs" type="button" data-toggle="collapse" data-target="#programsCollapse" aria-expanded="false" aria-controls="programsCollapse">
        <img src="@asset('images/Icons/Programs/programs-white75px.png')" alt="news icon" class="invisible">
        <i class="fa fa-chevron-right fa-1x"></i></button>
    </div>
  </div>
  <div class="collapse multi-collapse" id="programsCollapse">
    
      @php dynamic_sidebar('sidebar-programs') @endphp

  </div>
</div>
