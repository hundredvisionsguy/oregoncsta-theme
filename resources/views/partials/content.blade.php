<article @php post_class() @endphp>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{{ html_entity_decode(get_the_title()) }}</a></h2>
  </header>
  <div class="media">
    @php if ( has_post_thumbnail() ) {
	     the_post_thumbnail('thumbnail', ['class' => 'mr-3 align-self-center']);
     } @endphp
    <div class="media-body">
      <div class="entry-summary">
        @php the_excerpt() @endphp
      </div>
    </div>
  </div>
</article>
