@php
  $divclass = ' '.$class;
@endphp
<div class="home-content-container news row">
  <div class="home-content-nav">
    <div class="front">
      <h3 class="programs-toggle">News</h3>
      <i class="fa fa-1x fa-chevron-right"></i>
    </div>
    <div class="textwidget">
      <ul class="list-group list-group-flush">
      @foreach($news_loop as $news_item)
          <li class="list-group-item">{!! $news_item['link'] !!}{!! $news_item['title'] !!}</a></li>
      @endforeach
      </ul>
    </div>
  </div>
</div>
