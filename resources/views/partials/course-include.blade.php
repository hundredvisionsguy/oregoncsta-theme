<?php 
    //Expects the following variable to be set from including context:
    // $course_pod = pods( 'course', $post->ID );
    
    $session_types = get_the_terms( $post->ID, "session_type" );
    $experience_levels = get_the_terms( $post->ID, "experience_level" );
    $topics = get_the_terms( $post->ID, "topic" );
    $grade_levels = get_the_terms( $post->ID, "grade_level" );
?>

  <div class="course-content">
    @php the_content() @endphp

    <div class="course-info">
      <ul>

      @if ($experience_levels) 
        <li class="experience-levels"><span class="course-info-label">{{ count($experience_levels) > 1 ? "Experience Levels:" : "Experience Level:" }}</span>
        @foreach($experience_levels as $exp_level)
          @if ($loop->last)
            {{ $exp_level->name }}
          @else
            {{ $exp_level->name }}, 
          @endif
        @endforeach
        </li>
      @endif

      @if ($grade_levels) 
        <li class="grade-levels"><span class="course-info-label">{{ count($grade_levels) > 1 ? "Grade Levels:" : "Grade Level:" }}</span>
        @foreach($grade_levels as $grade_level)
          @if ($loop->last)
            {{ $grade_level->name }}
          @else
            {{ $grade_level->name }}, 
          @endif

        @endforeach
        </li>
      @endif

      @if ($topics) 
        <li class="topics"><span class="course-info-label">{{ count($topics) > 1 ? "Topics:" : "Topic:" }}</span>
        @foreach($topics as $topic)
          @if ($loop->last)
            {{ $topic->name }}
          @else
            {{ $topic->name }}, 
          @endif

        @endforeach
        </li>
      @endif

      @if ($session_types) 
        <li class="session-types"><span class="course-info-label">Session Type:</span>
          <ul>
          @foreach($session_types as $session_type_item)
            <li>{{ $session_type_item->name }}<br/><span class="session-type-description">{{ $session_type_item->description }}</span></li>
          @endforeach
          </ul>
        </li>
      @endif
      
      </ul>

    </div>

  </div>