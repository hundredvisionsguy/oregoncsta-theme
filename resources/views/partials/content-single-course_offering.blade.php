<?php 
    /* Initiate the Pods Object */
  	$mypod = pods( $post->post_type, $post->ID );
    $course = $mypod->field('course'); 
    $event = $mypod->field('event'); 
    $course_pod = pods( 'course', $course ); 
    $event_pod = pods( 'event', $event ); 

    $instructors = $mypod->field('instructor'); 
?>


<article @php post_class() @endphp>

  <div class="page-header">
    <h1>{{ $course['post_title'] }}</h1>
  </div>


  
  <div class="section-header"><h2>Place and Time:</h2></div>
  @php $url = htmlspecialchars_decode( $event['guid'] ); @endphp
  <a href="{{ $url }}">{{ $event['post_title'] }}<br/>
  </a>

  <!-- <div class="section-header"><h2>Location:</h2></div> -->
  <div class="offering-time">
  {{ $mypod->display('start_time') }}
  @if ($end_time)
      - {{ $mypod->display('end_time') }}
  @endif
  </div>

  <div class="section-header"><h2>Content:</h2></div>
  @php setup_postdata( get_post($course['ID']) );  @endphp
  @include('partials.course-include')
  @php wp_reset_postdata( );  @endphp


  @if ($instructors != false)
  @if (count($instructors) > 1)
  <div class="section-header"><h2>Instructors:</h2></div>
    <ul>
    @for($i = 0; $i < count($instructors); $i++)
      @php $url = htmlspecialchars_decode( $instructors[$i]['guid'] ); @endphp
      <li><a href="{{ $url }}">{{ $instructors[$i]['post_title'] }}</a></li>
    @endfor
    </ul>
  @elseif (count($instructors) == 1)
  <div class="section-header"><h2>Instructor:</h2></div>
      @php $url = htmlspecialchars_decode( $instructors[0]['guid'] ); @endphp
  <a href="{{ $url }}">{{ $instructors[0]['post_title'] }}</a>
  @endif
  @endif



  @if ($mypod->field('resources_link') !== "" )
  <div class="section-header"><h2>Resources:</h2></div>
  <a href="{{ $mypod->field('resources_link')  }}" target="_blank">Click here</a>
  @endif

  

</article>

