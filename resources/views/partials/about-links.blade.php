<div class="home-content-container row">
  <div class="home-content-nav closed row justify-content-between">
    <div class="col-9 col-md-12">
      <h3 class="about-link"><a href="about-us">About Us</a></h3>
    </div>
    <div class="col-3 col-md-12">
      <button id="aboutToggle" class="float-right toggle" type="button" data-toggle="collapse" data-target="#aboutCollapse" aria-expanded="false" aria-controls="aboutCollapse">
        <img src="@asset('images/Icons/About/about-white75px.png')" alt="about icon" class="invisible">
        <i class="fa fa-chevron-right fa-1x"></i></button>
    </div>
  </div>
  <div class="collapse multi-collapse" id="aboutCollapse">
    <div class="card">
      <ul class="list-group list-group-flush">
        <li class="list-group-item"><a href="about-us#ocsta">What is OCSTA</a></li>
        <li class="list-group-item"><a href="about-us#who-we-are">Who we are</a></li>
        <li class="list-group-item"><a href="about-us#what-we-do">What we do</a></li>
        <li class="list-group-item"><a href="about-us#join-us">Join Us</a></li>
      </ul>
    </div>
  </div>
</div>
