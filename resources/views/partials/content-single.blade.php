<article @php post_class() @endphp>
  <div class="page-header" >
    <h1>{{ html_entity_decode( get_the_title() ) }}</h1>
  </div>
  <div class="">
    <!--
      <img src="{{ $img_url[0] }}" alt="thumbnail of featured image" class="mr-3"/>
    -->

    <div class="featured-image">
    <?php the_post_thumbnail($size = [400,400]); ?>
    </div>

    <div class="media-body">
      <div class="entry-content">
        @php the_content() @endphp
        @include('partials/entry-meta')
      </div>

<!--
      <footer>
        {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
      </footer>
      @php /* comments_template('/partials/comments.blade.php') */ @endphp
-->
    </div>
  </div>
</article>
