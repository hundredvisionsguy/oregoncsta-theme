@php
  $divclass = ' '.$class;
@endphp
<div class="home-content-container<?php echo $divclass; ?> row">
  <div class="home-content-nav">
      @php
        dynamic_sidebar($id)
      @endphp
  </div>
</div>
