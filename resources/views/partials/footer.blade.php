<footer class="content-info">
  <div class="footer-containerXX container">
    <!-- <div style="width: 100%;"> -->
      <div class="row">
          <div class="footer-icon-container  col-12 col-md-5">
          @if (has_nav_menu('footer_icons'))  
            {!! wp_nav_menu(['theme_location' => 'footer_icons', 'menu_class' => 'nav']) !!}
          @endif
          </div>
          
          <!--
          @php dynamic_sidebar('sidebar-footer') @endphp
          -->

          <div class="national-branding col-12 col-md-7">
            <img id="oregoncsta-logo" height="24" src="@asset('images/CSTAOregon_white.png')" alt="CSTA National"/> is a chapter of <a href="http://csteachers.org">
            <img id="csta-logo" height="16" src="@asset('images/CSTA_white.png')" alt="CSTA National"/></a>
            
          </div>

          </div>
        <!-- </div> -->
    </div>
</footer>
