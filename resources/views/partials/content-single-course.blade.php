<?php /* Initiate the Pods Object */
    // get the current slug
    global $post;
  	// get pods object
    // $post->post_type can be used instead of the pod name, ie 'video'
    $mypod = pods( $post->post_type, $post->ID );
    
    $course_pod = $mypod;
?>

<article @php post_class() @endphp>

  @include('partials.page-header')


  @include('partials.course-include')

</article>
