<?php 

    global $post;

    //// /* Initiate the Pods Object */
    $mypod = pods( $post->post_type, $post->ID );
    
    $location = $mypod->field('location'); 
    $address = $mypod->field('address'); 
    $end_time = $mypod->field('end_time'); 

    $location_detail = $mypod->field('location_detail'); 
    $location_map = $mypod->field('location_map'); 

    //var_dump($mypod->fields['end_time']);
    //var_dump($mypod->display('end_date'));

    //-------------------------------------------------------------------------------------
    // Gather Course List

    /* Fills $offering_group with associative array of data:
        $offering_group[time] = array of offerings
    */

    $args = array(
      'posts_per_page'         => 100,
      'post_type'              => array( 'course_offering' ),
      'meta_query'             => array(
        'relation' => 'AND',
        array(
          'key'     => 'event',
          'value'   => $post->ID,
        ),
      ),
    );

    
    //include drafts and scheduled course offerings if showing a draft/scheduled event
    if( in_array($post->post_status, ['draft','future','pending']) )
      $args['post_status'] = ['publish','draft','future','pending'];

    $query = new WP_Query( $args );
    $course_offerings = $query->posts;

    //retrieve associated information
    foreach ( $course_offerings as $key => $offering ) {
        // This:
        $course_offerings[$key]->time = $offering->time;
        $course_offerings[$key]->course =  $associated_course = get_post_meta( $offering->ID, "course" );
        $course_offerings[$key]->instructors = get_post_meta( $offering->ID, "instructor" );
    }


    //Build up associative array of courses by time
    $offering_groups = array();
    foreach ( $course_offerings as $key => $offering ) {
      $courseTime = $offering->start_time;
      if($courseTime == '') {
        $courseTime = '_no_time';
      }
      $offering_groups[$courseTime][] = $offering;
    }

    //Sort groups by time
    function time_sort($a, $b) {
      //no time first
      if($a == '_no_time') return -1;
      if($b == '_no_time') return 1;
      
      //am before pm
      if( substr($a,-2) < substr($b, -2))
        return -1;
      if( substr($a,-2) > substr($b, -2))
        return 1;

      //same am/pm status - if needed, pad with leading 0
      $acolon = strpos($a, ":");
      if($acolon === 1) $a = "0" . $a;
      $bcolon = strpos($b, ":");
      if($bcolon === 1) $b = "0" . $b;

      //now can just compare resulting strings
      return $a > $b;
    }
    uksort($offering_groups, 'time_sort');
    //var_dump($offering_groups);

    // End Gather Course List
    //-------------------------------------------------------------------------------------


?>

<article @php post_class() @endphp>
  
    @include('partials.page-header')

    <div class="event-info">
    <!-- <div class="section-header"><h2>Location:</h2></div> -->
      @php $url = urlencode( $address ); @endphp
      <div class="location">
        <a href="https://www.google.com/maps/search/?api=1&query={{ $url }}" target="_blank" class="location">{{ $location }}</a>
      </div>
      

    
      <div class="location-detail">
      @if ($location_detail || $location_map)
          {{ $location_detail }}
          @if ($location_detail && $location_map)
           -  
          @endif
          @if ($location_map)
          <a href="{{ $location_map }}" target="_blank" class="location-map"> Facility Map </a>
          @else
          @endif
        @else
      @endif
      </div>

      <!-- <div class="section-header"><h2>Date{{ $mypod->display('end_date') !== '' ? "s" : "" }} :</h2></div> -->
      <div class="event-dates">
      {{ $mypod->display('start_date') }}{{ $mypod->display('end_date') !== '' ? " - " . $mypod->display('end_date') : "" }}
      </div>

    <!-- <div class="section-header"><h2>Location:</h2></div> -->
    <div class="event-times">
      {{ $mypod->display('start_time') }} 
      @if ($end_time) 
      - {{ $mypod->display('end_time') }}
      @endif
    </div>
  </div>
  
  <div class="the-content">
      @php the_content() @endphp
    </div>

    
    @if (count($offering_groups) > 0)
      <div class="section-header"><h2>Sessions:</h2></div>
      <div class="course-info">
      @foreach ( $offering_groups as $time_slot => $offerings_list )
          @if( $time_slot !== '_no_time' )
            <h3 class='event-course-offerings-time'>{{ $time_slot }}</h3>
          @endif
          <ul>
          @foreach ( $offerings_list as $offering ) 
            <li>
           @php $url = htmlspecialchars_decode( $offering->guid ) @endphp
            <a href="{{ $url }}">
            {{ $offering->course[0]['post_title'] }}
            </a></li>
          @endforeach
          </ul>
      @endforeach
      </div>
    @endif
  


    @php comments_template('/partials/comments.blade.php') @endphp

</article>
