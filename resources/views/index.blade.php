@extends('layouts.app')

@section('content')

  <!--
    TODO:
      add border between columns
      Question: do we want to make each section of the sidebar
      collapsible or not on mobile?
      I also say we don't put sidebar content above regular content
      (see SuperQuest mockups for mobile)
  -->
  <div class="row">
    <div class="col-xl-8">
      @include('partials.page-header')
      @if (!have_posts())
        <div class="alert alert-warning">
          {{ __('Sorry, no results were found.', 'sage') }}
        </div>
        {!! get_search_form(false) !!}
      @endif

      @while (have_posts()) @php the_post() @endphp
        @include('partials.content-'.get_post_type())
      @endwhile

      {!! get_the_posts_navigation() !!}

    </div>
    <div class="col-xl-4">
      @include('partials/sidebar')
    </div>

@endsection
