{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  <div class="justify-content-center">
     
    <div class="row">
      @if ($post->post_content !== "")
         <div class="col-lg-12 col-12">
          @php
            echo do_shortcode('[metaslider id="3181"]')
          @endphp
        </div>
        <div class="col-lg-12 col-12 about-us-blurb">
          @while(have_posts()) @php the_post() @endphp
            @include('partials.page-header')
            @include('partials.content-page')
            @yield('content')
            <div class="homepage-buttons" style="text-align: center;">
            
              @if (has_nav_menu('action_buttons'))  
                {!! wp_nav_menu(['theme_location' => 'action_buttons', 'menu_class' => 'action_buttons']) !!}
              @endif

            </div>
          @endwhile
        </div>
      @else
        @php
          echo do_shortcode('[metaslider id="3181"]')
        @endphp
      @endif
        </div>


    </div>
    <div id="home-widget-container" class="">
    @include('partials.event-grid', ['id' => 'event-grid', 'class' => 'event'])
    @include('partials.blog-grid', ['id' => 'blog-grid', 'class' => 'blog'])
    </div>
    
  <!--
    For future use
    <div id="home-widget-container" class="">
      @include('partials.event-grid', ['id' => 'event-grid', 'class' => 'event'])
    <div>
  -->

    <div>
    @php(do_action('get_footer'))
  </div>

@endsection
