@extends('layouts.app')

@section('content')

  <div class="row">
    <div class="col-12">
      @include('partials.page-header')

      @if (!have_posts())
        <div class="alert alert-warning">
          {{ __('Sorry, no results were found.', 'sage') }}
        </div>
        {!! get_search_form(false) !!}
      @endif

      <h2>Upcoming:</h2>
      <?php 
        global $post;
        $args = array(
          'posts_per_page'   => 10,
          'orderby'          => 'meta_value',
          'order'          	 => 'ASC',
          'meta_key'         => 'start_date',
          'post_type'        => 'event',
          'meta_query' => array(
            array('key' => 'start_date',
                  'value' => date("Y-m-d"),
                  'compare' => '>='
                )
            )
        );
        $upcoming_events = get_posts( $args );
        
        foreach ( $upcoming_events as $post_obj ) {
          $post = $post_obj;
          setup_postdata( $post );
          ?>
          @include('partials.content-event')
          <?php
        }
      ?>

      <hr/>

      <h2>Past:</h2>

        @while (have_posts()) @php the_post() @endphp
          @include('partials.content-'.get_post_type())
        @endwhile

        {!! get_the_posts_navigation( array(
            'prev_text' => __('Older events', 'theme_textdomain'),
            'next_text' => __('Newer events', 'theme_textdomain'),
          )
        ) !!}

    </div>
  </div>

@endsection
