export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    // add about-link class
    $('.nav-primary li:contains("About")').addClass("about-link");
    $('.nav-primary li:contains("News")').addClass("news-link");
    $('.nav-primary li:contains("Programs")').addClass("programs-link");
    $('.nav-primary li:contains("Resources")').addClass("resources-link");
    $('.nav li:contains("Log")').addClass("login-link");
    $('.nav li:contains("Face")').addClass("facebook-link");
    $('.nav li:contains("Twitter")').addClass("twitter-link");
    $('.nav li:contains("Instagram")').addClass("instagram-link");
  },
};
