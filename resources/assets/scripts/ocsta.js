$(document).ready(function() {
  // set some global variables
  var programs_duration, news_duration, resources_duration, about_duration;
  // Show menu toggle if width is less than 768px
  if (window.innerWidth >= 768) {
    $('img.menu-toggle').hide(0);
    $('#newsToggle img').toggleClass('invisible');
    $('#newsToggle i').toggleClass('invisible');
    $('#aboutToggle img').toggleClass('invisible');
    $('#aboutToggle i').toggleClass('invisible');
    $('#programsToggle img').toggleClass('invisible');
    $('#programsToggle i').toggleClass('invisible');
    $('#resourcesToggle img').toggleClass('invisible');
    $('#resourcesToggle i').toggleClass('invisible');
    // hide all i toggles
    $('.home-content-container i').hide(0);
  }
  else {
    $('img.menu-toggle').show(0);
    $('.home-content-container i').show(0);
  }
  $('img.menu-toggle').click(function() {
    $(this).toggleClass("open");
    $(this).find('.home-content-nav').toggleClass("hidden-background");
    $('.nav-primary').toggleClass("open");
  });
  // Hide text widget (regardless of browser size)
  $('.textwidget').hide();
  $('div.footer-container .textwidget').show(); //but show footer

  // deal with resizing
  $(window).resize(function() {
    //$('.home-content-container.open .textwidget').hide(10);
    if (window.innerWidth >= 768) {
      $('img.menu-toggle').fadeOut();
      $('#newsToggle img').removeClass('invisible');
      $('#newsToggle i').addClass('invisible');
      $('#aboutToggle img').removeClass('invisible');
      $('#aboutToggle i').addClass('invisible');
      $('#programsToggle img').removeClass('invisible');
      $('#programsToggle i').addClass('invisible');
      $('#resourcesToggle img').removeClass('invisible');
      $('#resourcesToggle i').addClass('invisible');
      $('.home-content-container i').hide();
    }
    else {
      $('img.menu-toggle').fadeIn();
      $('#newsToggle img').addClass('invisible');
      $('#newsToggle i').removeClass('invisible');
      $('#aboutToggle img').addClass('invisible');
      $('#aboutToggle i').removeClass('invisible');
      $('#programsToggle img').addClass('invisible');
      $('#programsToggle i').removeClass('invisible');
      $('#resourcesToggle img').addClass('invisible');
      $('#resourcesToggle i').removeClass('invisible');
      $('.home-content-container i').show();
    }
  });

  // Add events class to navbar
  $("#menu-top-navigation li a:contains('Events')").parent().addClass('event-link');
  
  // home-content content containers
  $('.textwidget').addClass('collapse');
  // home-content toggles

  programs_duration = news_duration = resources_duration = about_duration = 500;
  $('.home-content-container.programs').on( "click", {widget: '.programs', duration: programs_duration}, toggleTextWidget);
  $('.home-content-container.news').on( "click", {widget: '.news', duration: news_duration}, toggleTextWidget);
  $('.home-content-container.resources').on( "click", {widget: '.resources', duration: resources_duration}, toggleTextWidget);
  $('.home-content-container.about').on( "click", {widget: '.about', duration: about_duration}, toggleTextWidget);

  $('.home-content-container button').click(function() {
    $(this).children().toggleClass('fa-chevron-down fa-chevron-right');
    $(this).parent().parent().toggleClass('open closed')
  });
});

function toggleTextWidget(event) {
  //alert("widget: " + widget);
  $(event.data.widget).toggleClass('open');
  $(event.data.widget).find('.textwidget').slideToggle(event.data.duration, function(){
    if ($(window).width() >= 768)
      event.data.duration = $(this).is(":visible") ? 0 : 500;
    else
      event.data.duration = 500;
    // alert('duration is now' + duration);
  });
  $(event.data.widget).find('i').toggleClass('fa-chevron-down fa-chevron-right');
}
