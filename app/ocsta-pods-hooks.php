<?php
/**
 * Included from functions.php
 * 
 * OCSTA Specific hooks & overrides
 */




//Load OCSTA pods hooks only if PODS plugin is active
//  after_setup_theme is first available hook for theme files
add_action( 'after_setup_theme', 'ocsta_pods_hooks_extend_safe_activate');
function ocsta_pods_hooks_extend_safe_activate() {
	if ( defined( 'PODS_VERSION' ) ) {
        add_filter('pods_api_pre_save_pod_item_course_offering', 'course_offering_title_generator', 10, 2); 
	}
}


/**
 * 
 */
function course_offering_title_generator($pieces, $is_new_item) { 

    $course_title = get_the_title($pieces[ 'fields' ][ 'course' ][ 'value' ]);

    $event_title = "NO EVENT";
    $event_id = $pieces[ 'fields' ][ 'event' ][ 'value' ];
    if($event_id != '')
        $event_title = get_the_title($event_id);
    $time_string = "";
    $offering_time = $pieces[ 'fields' ][ 'start_time' ][ 'value' ];
    if($offering_time != '')
        $time_string = '-' . $offering_time;

    $args = array();
    $args['ID'] = $pieces[ 'params' ]->id;
    $args['post_title'] = $event_title . "-" . $course_title . $time_string;

    if($is_new_item)
        $args['post_name'] = $args['post_title'];

    wp_update_post( $args );

    return $pieces; 
}  
