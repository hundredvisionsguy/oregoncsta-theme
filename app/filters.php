<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
}, 100);

// See the __() WordPress function for valid values for $text_domain.
// Homepage sidebars
add_filter('Footer_title','Footer_title_remove'); 
function Footer_title_remove($t)
{
    return null;
}
register_sidebar( array(
    'id'          => 'programs-sidebar',
    'name'        => __( 'Programs Widget', 'oregon-csta' ),
    'description' => __( 'This sidebar is tile "card" to display programs.', 'oregon-csta' ),
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<div class="front"><h3 class="programs-toggle">',
    'after_title'   => '</h3><i class="fa fa-chevron-right fa-1x"></i></div>'
) );
register_sidebar( array(
    'id'          => 'resources-sidebar',
    'name'        => __( 'Resources Widget', 'oregon-csta' ),
    'description' => __( 'This sidebar is tile "card" to display OCSTA resources.', 'oregon-csta' ),
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<div class="front"><h3 class="resources-toggle">',
    'after_title'   => '</h3><i class="fa fa-chevron-right fa-1x"></i></div>'
) );
register_sidebar( array(
    'id'          => 'about-sidebar',
    'name'        => __( 'About Widget', 'oregon-csta' ),
    'description' => __( 'This sidebar is tile "card" to display information about OCSTA.', 'oregon-csta' ),
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<div class="front"><h3 class="about-toggle">',
    'after_title'   => '</h3><i class="fa fa-chevron-right fa-1x"></i></div>'
) );

/* Change the archive titles */
add_filter('get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
});

add_filter('nav_menu_css_class', function ($classes, $item, $args) {
    if($args->theme_location == 'action_buttons') {
        $classes[] = 'btn btn-lg btn-success action-button';
    }
    return $classes;
}, 10, 3);


/*modify events archive to only show past events in default loop*/
add_action( 'pre_get_posts', function ( $query ) {
    if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'event' ) ) {
        $query->set( 'posts_per_page', '6' );
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'meta_key', 'start_date' );
        $query->set( 'order', 'DESC' );
        $query->set( 'meta_query', array(
            array('key' => 'start_date',
                  'value' => date("Y-m-d"),
                  'compare' => '<'
                )
            )
        );
    }
});