<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
  public function newsLoop()
  {
    $news_items = get_posts([
      'post_type' => 'post',
      'category_name' => 'news',
      'posts_per_page' => '3'
    ]);
    return array_map(function ($post) {
      $text = strip_shortcodes( $post->post_content );
      $text = apply_filters( 'the_content', $text );
      $text = str_replace(']]>', ']]&gt;', $text);
      $excerpt_length = apply_filters( 'excerpt_length', 55 );
      $excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
      $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );

      return [
          'title' => apply_filters('the_title', $post->post_title),
          'link' => '<a href="'.get_the_permalink($post->ID).'">',
          'excerpt' => $text,
          'thumbnail' => get_the_post_thumbnail_url($post->ID, 'medium'),
      ];
    }, $news_items);
  }

  public function eventsLoop()
  {
    //Build a list of events for front page.
    //Will look best if (count(list) + 1) % 3 == 0

    //Get next up to 5 items events
    $events_items = get_posts([
      'posts_per_page' => '5',
      'post_type' => 'event',
      'order'     => 'ASC',
      'meta_key' => 'start_date',
      'orderby'   => 'meta_value', //or 'meta_value_num'
      'meta_query' => array(
                          array('key' => 'start_date',
                                'value' => date("Y-m-d"),
                                'compare' => '>='
                          )
                      )
    ]);

    //If < 2 events upcoming, pad list with older ones...
    if( count($events_items) < 2 ) {
      $old_events = get_posts([
        'posts_per_page' => 2 - count($events_items),
        'post_type' => 'event',
        'order'     => 'DESC',
        'meta_key' => 'start_date',
        'orderby'   => 'meta_value', //or 'meta_value_num'
        'meta_query' => array(
                            array('key' => 'start_date',
                                  'value' => date("Y-m-d"),
                                  'compare' => '<'
                            )
                        )
      ]);

      $events_items = array_merge($events_items, $old_events);
    }

    return array_map(function ($post) {
      return [
          'title' => apply_filters('the_title', $post->post_title),
          'link' => '<a href="'.get_the_permalink($post->ID).'">',
          'thumbnail' => get_the_post_thumbnail_url($post->ID, 'medium'),
      ];
    }, $events_items);

  }

  public function programsLoop()
  {
    $programs_items = get_posts([
      'post_type' => 'post',
      'category_name' => 'programs'
    ]);
    return array_map(function ($post) {
      $text = strip_shortcodes( $post->post_content );
      $text = apply_filters( 'the_content', $text );
      $text = str_replace(']]>', ']]&gt;', $text);
      $excerpt_length = apply_filters( 'excerpt_length', 55 );
      $excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
      $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
      return [
          'title' => apply_filters('the_title', $post->post_title),
          'link' => '<a href="'.get_the_permalink($post->ID).'">',
          'excerpt' => $text,
      ];
    }, $programs_items);
  }
  public function resourcesLoop()
  {
    $resources_items = get_posts([
      'post_type' => 'post',
      'category_name' => 'resources'
    ]);
    return array_map(function ($post) {
      $text = strip_shortcodes( $post->post_content );
      $text = apply_filters( 'the_content', $text );
      $text = str_replace(']]>', ']]&gt;', $text);
      $excerpt_length = apply_filters( 'excerpt_length', 55 );
      $excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
      $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
      return [
          'title' => apply_filters('the_title', $post->post_title),
          'link' => '<a href="'.get_the_permalink($post->ID).'">',
          'excerpt' => $text,
      ];
    }, $resources_items);
  }
  public function img_url() {
      return wp_get_attachment_image_src(get_post_thumbnail_id(), [290, 190], false);
  }
}
